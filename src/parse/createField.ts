import { Parameter } from "./parsedModel";
import { Import } from "./impexp/import";

export function createField(name: string, typeName: string, imports: Map<string, Import>): Parameter {
  name = name.trim();
  typeName = typeName.trim();
  const isOptional = name.endsWith("?");
  if (isOptional) {
    name = name.substr(0, name.length - 1);
  }

  return {
    name,
    typeName,
    isOptional,
    imports: getImports(typeName, imports)
  };
}

export function getImports(typeName: string, imports: Map<string, Import>): Import[] {
  const split = typeName.replace(/[\[|\]|\s|\t]/g, "").split(/[<|>]/);
  return split.map(type => imports.get(type.trim())).filter(type => type);
}

