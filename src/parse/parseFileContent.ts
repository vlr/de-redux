import { ParsedReductionsFile, ParsedStatesFile } from "./parsedModel";
import { parseImports } from "./impexp/parseImports";
import { parseStates } from "./parseStates";
import { parseReductions } from "./parseReductions";
import { map } from "@vlr/map-tools";
import { splitPath } from "../tools/splitPath";
import { stripExtension } from "../tools/stripPath";

export interface CombinedParsedFile extends ParsedReductionsFile, ParsedStatesFile {
}

export function parseFileContent(file: string, content: string, tsconfig: any): CombinedParsedFile {
  const split = splitPath(file);
  const imports = map(parseImports(tsconfig, content, split.path), i => i.aliasName);
  return {
    path: split.path,
    fileName: stripExtension(split.file),
    states: parseStates(content, imports),
    reductions: parseReductions(content, imports)
  };
}
