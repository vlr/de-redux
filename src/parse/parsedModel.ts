import { Import } from "./impexp/import";

export interface ParsedFile {
  path: string;
  fileName: string;
}

export interface ParsedStatesFile extends ParsedFile {
  states: ParsedState[];
}

export interface ParsedState {
  name: string;
  fields: Parameter[];
}

export interface ParsedReductionsFile extends ParsedFile {
  reductions: ParsedReduction[];
}


export interface ParsedReduction {
  reducted: Import;
  partialName: string;
  name: string;
  parameters: Parameter[];
}

export interface Parameter {
  name: string;
  isOptional: boolean;
  typeName: string;
  imports: Import[];
}

export interface ParsedModel {
  stateFiles: ParsedStatesFile[];
  reductionFiles: ParsedReductionsFile[];
}
