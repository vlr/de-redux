import { regexToArray } from "@vlr/array-tools";

export function containsReexports(content: string): boolean {
  return getExportedNames(content).length > 0;
}

const funcRegex = /export[\s]*function[\s]*([^\s^\()]*)|export[\s]*async[\s]*function[\s]*([^\s^\()]*)/g;
const typeRegex = /export[\s]*enum[\s]*([^\s^{]*)|export[\s]*type[\s]*([^\s^{]*)/g;
const classRegex = /export[\s]*interface[\s]*([^\s^{]*)|export[\s]*class[\s]*([^\s^{]*)/g;
const constRegex = /export[\s]*const[\s]*([^\s]*)[\s]*[:[\s]*[^\s]*[\s]*]?=/g;
export function getExportedNames(content: string): string[] {
  const funcs = regexToArray(funcRegex, content).map(match => match[1] || match[2]);
  const types = regexToArray(typeRegex, content).map(match => match[1] || match[2]);
  const classes = regexToArray(classRegex, content).map(match => match[1] || match[2]);
  const constants = regexToArray(constRegex, content).map(match => match[1]);
  return [...funcs, ...types, ...classes, ...constants];
}
