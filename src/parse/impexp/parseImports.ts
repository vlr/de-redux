import { calculateRealPath } from "../path/calculateRealPath";
import { Import } from "./import";
import { regexToArray, flatMap } from "@vlr/array-tools";

export function parseImports(tsConfig: any, content: string, path: string): Import[] {
  return internalImports(content).concat(externalImports(tsConfig, content, path));
}

const exportRegex = /export interface (.*) {/g;
function internalImports(content: string): Import[] {
  const matches = regexToArray(exportRegex, content);
  return matches.map(match => ({ typeName: match[1], aliasName: match[1], sameFile: true }));
}

const importRegex = /import[\s]*{(.*)}[\s]*from[\s]*["|"](.*)["|"]/g;
function externalImports(tsConfig: any, content: string, path: string): Import[] {
  const matches = regexToArray(importRegex, content);
  return flatMap(matches, match => parseMatch(tsConfig, match[1], match[2], path));
}

function parseMatch(tsConfig: any, types: string, importline: string, path: string): Import[] {
  const realPath = calculateRealPath(tsConfig, path, importline);
  return types.split(",").map(type => createImport(type, realPath)).filter(x => x);
}

function createImport(type: string, realPath: string): Import {
  const parts = type.split(" ").filter(s => s.length);
  if (!parts.length) {
    return null;
  }
  const index = parts.length === 3 && parts[1] === "as" ? 2 : 0;
  return {
    typeName: parts[0],
    aliasName: parts[index],
    realPath,
    sameFile: false
  };
}
