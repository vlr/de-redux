import { flatMap } from "@vlr/array-tools";
import { combinePath } from "../tools/combinePath";

const reductionPostfix = "**/*.reduction.ts";
const statePostfix = "**/*.state.ts";

export function createGlobs(paths: string | string[]): string[] {
  if (Array.isArray(paths)) {
    return flatMap(paths, path => createTwoGlobs(path));
  } else {
    return createTwoGlobs(paths);
  }
}

function createTwoGlobs(path: string): string[] {
  path = path.replace(/\\/g, "/");
  return [combinePath(path, statePostfix), combinePath(path, reductionPostfix)];
}
