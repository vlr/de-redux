import { regexToArray } from "@vlr/array-tools";
import { ParsedReduction, Parameter } from "./parsedModel";
import { Import } from "./impexp/import";
import { createField } from "./createField";
import { splitParameters } from "./splitParameters";
import { parsePartialReductions } from "./parsePartialReductions";


export function parseReductions(content: string, imports: Map<string, Import>): ParsedReduction[] {
  return parseImmediateReductions(content, imports).concat(parsePartialReductions(content, imports));
}

const funcRegex = /export\s*function\s*([^\(\s]*)\s*?\(([^\)]*)\): ([^{\s]*)\s*?{/g;
export function parseImmediateReductions(content: string, imports: Map<string, Import>): ParsedReduction[] {
  const matches = regexToArray(funcRegex, content);
  return matches.map(match => toReduction(match, imports)).filter(r => r);
}


export function toReduction(match: string[], imports: Map<string, Import>): ParsedReduction {
  const [, name, args, returnTypeName] = match;

  const parameters = parseParameters(args, imports);
  if (!parameters.length || returnTypeName !== parameters[0].typeName || parameters[0].imports.length !== 1) {
    return null;
  }

  return {
    reducted: parameters[0].imports[0],
    partialName: null,
    name,
    parameters: parameters.slice(1),
  };
}

function parseParameters(args: string, imports: Map<string, Import>): Parameter[] {
  const split = splitParameters(args);
  return split.map(parm => createParameter(parm, imports));
}

function createParameter(parm: string, imports: Map<string, Import>): Parameter {
  const split = parm.split(":");
  return createField(split[0], split[1], imports);
}
