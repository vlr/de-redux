import { ParsedModel } from "./parsedModel";
import { createGlobs } from "./createGlobs";
import * as glob from "globby";
import * as fse from "fs-extra";
import { parseFileContent, CombinedParsedFile } from "./parseFileContent";

export async function parseFiles(folders: string | string[], tsconfig: any): Promise<ParsedModel> {
  const files = await glob(createGlobs(folders));
  const parsed = await Promise.all(files.map(file => parseFile(file, tsconfig)));
  const stateFiles = parsed.filter(file => file.states.length);
  const reductionFiles = parsed.filter(file => file.reductions.length);
  return {
    stateFiles,
    reductionFiles
  };
}

async function parseFile(file: string, tsconfig: any): Promise<CombinedParsedFile> {
  const content = await fse.readFile(file, "utf8");
  return parseFileContent(file, content, tsconfig);
}
