import { regexToArray } from "@vlr/array-tools";
import { ParsedState } from "./parsedModel";
import { Import } from "./impexp/import";
import { createField } from "./createField";

export function parseStates(content: string, imports: Map<string, Import>): ParsedState[] {
  return parseInterfaces(content, imports).concat(parseReadonlyTypes(content, imports));
}

const interfaceRegex = /export\s*interface\s*(.*)\s*?{\r?\n((?:.*?\r?\n?)*?)\s*?}/g;
export function parseInterfaces(content: string, imports: Map<string, Import>): ParsedState[] {
  const matches = regexToArray(interfaceRegex, content);
  return matches.map(match => parseState(match[1], match[2], imports));
}

const typeRegex = /\s*?export\s*type\s*([^\s]*)\s*?=\s*?(?:Recursive)?Readonly<{\s*?\r?\n((?:.*?\r?\n)*?)\s*?}>;/g;
export function parseReadonlyTypes(content: string, imports: Map<string, Import>): ParsedState[] {
  const matches = regexToArray(typeRegex, content);
  return matches.map(match => parseState(match[1], match[2], imports));
}


const fieldRegex = /(.*):([^;]*);?/g;
export function parseState(name: string, content: string, imports: Map<string, Import>): ParsedState {
  name = name.trim().split(" ")[0];
  const matches = regexToArray(fieldRegex, content);
  const fields = matches.map(match => createField(match[1], match[2], imports));
  return {
    name,
    fields
  };
}
