export function getFilePath(path: string): string {
  return stripToLast(path, "/");
}

export function stripExtension(fullName: string): string {
  return stripToLast(fullName, ".");
}

function stripToLast(input: string, symbol: string): string {
  const index = input.lastIndexOf(symbol);
  return input.substr(0, index);
}
