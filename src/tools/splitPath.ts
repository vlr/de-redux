import { getFilePath } from "./stripPath";

export interface SplitPath {
  path: string;
  file: string;
}

export function splitPath(fullPath: string): SplitPath {
  const path = getFilePath(fullPath);
  const file = fullPath.substr(path.length + 1);
  return {
    path,
    file
  };
}
