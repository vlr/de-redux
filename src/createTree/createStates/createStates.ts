import { ParsedModel } from "../../parse/parsedModel";
import { State } from "../tree";
import { createState } from "./createState";
import { createStateId } from "../createStateId";
import { flatMap } from "@vlr/array-tools";

export function createStates(parsed: ParsedModel): State[] {
  const allIds = getAllStateIds(parsed);
  return flatMap(parsed.stateFiles, file => file.states.map(state => createState(state, file, allIds)));
}

function getAllStateIds(parsed: ParsedModel): Set<string> {
  const stateIds = flatMap(parsed.stateFiles, file => file.states.map(state => createStateId(state, file)));
  return new Set(stateIds);
}
