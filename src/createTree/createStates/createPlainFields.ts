import { Parameter } from "../../parse/parsedModel";
import { PlainField } from "../tree";
import { isState } from "./isState";

export function createPlainFields(parsed: Parameter[], allIds: Set<string>, fileStates: string[]): PlainField[] {
  return parsed.filter(param => isState(param, allIds, fileStates))
    .map(param => param);
}
