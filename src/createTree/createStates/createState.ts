import { State } from "../tree";
import { ParsedState, ParsedStatesFile } from "../../parse/parsedModel";
import { createStateId } from "../createStateId";
import { createPlainFields } from "./createPlainFields";
import { createStateFields } from "./createStateFields";

export function createState(state: ParsedState, file: ParsedStatesFile, allIds: Set<string>): State {
  const fileStates = file.states.map(state => state.name);
  const plainFields = createPlainFields(state.fields, allIds, fileStates);
  const stateFields = createStateFields(state.fields, allIds, fileStates);
  return {
    name: state.name,
    stateId: createStateId(state, file),
    plainFields,
    stateFields
  };
}
