import { Parameter } from "../../parse/parsedModel";
import { combinePath } from "../../tools/combinePath";

export function paramStateId(param: Parameter): string {
  return combinePath(param.imports[0].typeName, param.typeName);
}
