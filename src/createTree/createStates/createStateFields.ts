import { Parameter } from "../../parse/parsedModel";
import { StateField } from "../tree";
import { isState } from "./isState";
import { paramStateId } from "./paramStateId";

export function createStateFields(parsed: Parameter[], allIds: Set<string>, fileStates: string[]): StateField[] {
  return parsed.filter(param => isState(param, allIds, fileStates))
    .map(param => createStateField(param));
}

function createStateField(param: Parameter): StateField {
  return {
    stateId: paramStateId(param),
    ...param
  };
}
