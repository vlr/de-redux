import { Parameter } from "../../parse/parsedModel";
import { isComplexType } from "../../tools/isComplexType";
import { paramStateId } from "./paramStateId";

export function isState(parameter: Parameter, idsSet: Set<string>, fileStates: string[]): boolean {
  if (isComplexType(parameter.typeName)) {
    return false;
  }

  switch (parameter.imports.length) {
    case 0:
      return false;
    case 1:
      return isSameFileState(parameter) || isImportedState(parameter, idsSet);
    default:
      throw new Error(`non-complex type ${parameter.typeName} in parameter ${parameter.name} has more than one imports`);
  }
}

function isSameFileState(parameter: Parameter): boolean {
  return parameter.imports[0].sameFile;
}

function isImportedState(parameter: Parameter, idsSet: Set<string>): boolean {
  return idsSet.has(paramStateId(parameter));
}
