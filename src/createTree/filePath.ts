import { ParsedFile } from "../parse/parsedModel";
import { combinePath } from "../tools/combinePath";

export function filePath(file: ParsedFile): string {
  return combinePath(file.path, file.fileName);
}
