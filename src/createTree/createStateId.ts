import { ParsedStatesFile, ParsedState } from "../parse/parsedModel";
import { filePath } from "./filePath";
import { combinePath } from "../tools/combinePath";

export function createStateId(state: ParsedState, file: ParsedStatesFile): string {
  return combinePath(filePath(file), state.name);
}
