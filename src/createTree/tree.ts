import { Parameter } from "../parse/parsedModel";

export interface Tree {
  nodes: TreeNode[];
}

export interface TreeNode {
  id: string;
  state: State;
  parentId: string;
  fieldName: string;
}

export interface State {
  stateId: string;
  name: string;
  plainFields: PlainField[];
  stateFields: StateField[];
}

export interface PlainField extends Parameter {

}

export interface StateField extends Parameter {
  stateId: string;
}
