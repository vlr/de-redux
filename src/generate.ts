import { parseFiles } from "./parse/parseFiles";
import { Options, IdType } from "./options";
import { createTree } from "./createTree/createTree";

const defaultOptions: Options = {
  lineFeed: "\r\n",
  IdType: IdType.number
};

export async function generate(paths: string | string[], options: Options = {}, tsconfig: any = {}): Promise<void> {
  options = {
    ...defaultOptions,
    ...options
  };

  const parsed = await parseFiles(paths, tsconfig);
  const tree = createTree(parsed);
  console.log(tree);
}
