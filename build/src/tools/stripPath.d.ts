export declare function getFilePath(path: string): string;
export declare function stripExtension(fullName: string): string;
