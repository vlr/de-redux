"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const stripPath_1 = require("./stripPath");
function splitPath(fullPath) {
    const path = stripPath_1.getFilePath(fullPath);
    const file = fullPath.substr(path.length + 1);
    return {
        path,
        file
    };
}
exports.splitPath = splitPath;
//# sourceMappingURL=splitPath.js.map