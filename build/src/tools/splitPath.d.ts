export interface SplitPath {
    path: string;
    file: string;
}
export declare function splitPath(fullPath: string): SplitPath;
