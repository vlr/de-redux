"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getFilePath(path) {
    return stripToLast(path, "/");
}
exports.getFilePath = getFilePath;
function stripExtension(fullName) {
    return stripToLast(fullName, ".");
}
exports.stripExtension = stripExtension;
function stripToLast(input, symbol) {
    const index = input.lastIndexOf(symbol);
    return input.substr(0, index);
}
//# sourceMappingURL=stripPath.js.map