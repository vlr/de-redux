"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const array_tools_1 = require("@vlr/array-tools");
const createField_1 = require("./createField");
const splitParameters_1 = require("./splitParameters");
const parsePartialReductions_1 = require("./parsePartialReductions");
function parseReductions(content, imports) {
    return parseImmediateReductions(content, imports).concat(parsePartialReductions_1.parsePartialReductions(content, imports));
}
exports.parseReductions = parseReductions;
const funcRegex = /export\s*function\s*([^\(\s]*)\s*?\(([^\)]*)\): ([^{\s]*)\s*?{/g;
function parseImmediateReductions(content, imports) {
    const matches = array_tools_1.regexToArray(funcRegex, content);
    return matches.map(match => toReduction(match, imports)).filter(r => r);
}
exports.parseImmediateReductions = parseImmediateReductions;
function toReduction(match, imports) {
    const [, name, args, returnTypeName] = match;
    const parameters = parseParameters(args, imports);
    if (!parameters.length || returnTypeName !== parameters[0].typeName || parameters[0].imports.length !== 1) {
        return null;
    }
    return {
        reducted: parameters[0].imports[0],
        partialName: null,
        name,
        parameters: parameters.slice(1),
    };
}
exports.toReduction = toReduction;
function parseParameters(args, imports) {
    const split = splitParameters_1.splitParameters(args);
    return split.map(parm => createParameter(parm, imports));
}
function createParameter(parm, imports) {
    const split = parm.split(":");
    return createField_1.createField(split[0], split[1], imports);
}
//# sourceMappingURL=parseReductions.js.map