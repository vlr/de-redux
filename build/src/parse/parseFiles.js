"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const createGlobs_1 = require("./createGlobs");
const glob = require("globby");
const fse = require("fs-extra");
const parseFileContent_1 = require("./parseFileContent");
async function parseFiles(folders, tsconfig) {
    const files = await glob(createGlobs_1.createGlobs(folders));
    const parsed = await Promise.all(files.map(file => parseFile(file, tsconfig)));
    const stateFiles = parsed.filter(file => file.states.length);
    const reductionFiles = parsed.filter(file => file.reductions.length);
    return {
        stateFiles,
        reductionFiles
    };
}
exports.parseFiles = parseFiles;
async function parseFile(file, tsconfig) {
    const content = await fse.readFile(file, "utf8");
    return parseFileContent_1.parseFileContent(file, content, tsconfig);
}
//# sourceMappingURL=parseFiles.js.map