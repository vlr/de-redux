import { Parameter } from "./parsedModel";
import { Import } from "./impexp/import";
export declare function createField(name: string, typeName: string, imports: Map<string, Import>): Parameter;
export declare function getImports(typeName: string, imports: Map<string, Import>): Import[];
