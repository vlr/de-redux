import { ParsedState } from "./parsedModel";
import { Import } from "./impexp/import";
export declare function parseStates(content: string, imports: Map<string, Import>): ParsedState[];
export declare function parseInterfaces(content: string, imports: Map<string, Import>): ParsedState[];
export declare function parseReadonlyTypes(content: string, imports: Map<string, Import>): ParsedState[];
export declare function parseState(name: string, content: string, imports: Map<string, Import>): ParsedState;
