import { ParsedModel } from "./parsedModel";
export declare function parseFiles(folders: string | string[], tsconfig: any): Promise<ParsedModel>;
