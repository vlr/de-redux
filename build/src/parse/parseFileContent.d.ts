import { ParsedReductionsFile, ParsedStatesFile } from "./parsedModel";
export interface CombinedParsedFile extends ParsedReductionsFile, ParsedStatesFile {
}
export declare function parseFileContent(file: string, content: string, tsconfig: any): CombinedParsedFile;
