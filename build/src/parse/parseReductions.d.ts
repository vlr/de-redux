import { ParsedReduction } from "./parsedModel";
import { Import } from "./impexp/import";
export declare function parseReductions(content: string, imports: Map<string, Import>): ParsedReduction[];
export declare function parseImmediateReductions(content: string, imports: Map<string, Import>): ParsedReduction[];
export declare function toReduction(match: string[], imports: Map<string, Import>): ParsedReduction;
