import { Import } from "./impexp/import";
import { ParsedReduction } from "./parsedModel";
export declare function parsePartialReductions(content: string, imports: Map<string, Import>): ParsedReduction[];
