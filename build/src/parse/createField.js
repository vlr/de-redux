"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function createField(name, typeName, imports) {
    name = name.trim();
    typeName = typeName.trim();
    const isOptional = name.endsWith("?");
    if (isOptional) {
        name = name.substr(0, name.length - 1);
    }
    return {
        name,
        typeName,
        isOptional,
        imports: getImports(typeName, imports)
    };
}
exports.createField = createField;
function getImports(typeName, imports) {
    const split = typeName.replace(/[\[|\]|\s|\t]/g, "").split(/[<|>]/);
    return split.map(type => imports.get(type.trim())).filter(type => type);
}
exports.getImports = getImports;
//# sourceMappingURL=createField.js.map