"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const array_tools_1 = require("@vlr/array-tools");
const createField_1 = require("./createField");
function parseStates(content, imports) {
    return parseInterfaces(content, imports).concat(parseReadonlyTypes(content, imports));
}
exports.parseStates = parseStates;
const interfaceRegex = /export\s*interface\s*(.*)\s*?{\r?\n((?:.*?\r?\n?)*?)\s*?}/g;
function parseInterfaces(content, imports) {
    const matches = array_tools_1.regexToArray(interfaceRegex, content);
    return matches.map(match => parseState(match[1], match[2], imports));
}
exports.parseInterfaces = parseInterfaces;
const typeRegex = /\s*?export\s*type\s*([^\s]*)\s*?=\s*?(?:Recursive)?Readonly<{\s*?\r?\n((?:.*?\r?\n)*?)\s*?}>;/g;
function parseReadonlyTypes(content, imports) {
    const matches = array_tools_1.regexToArray(typeRegex, content);
    return matches.map(match => parseState(match[1], match[2], imports));
}
exports.parseReadonlyTypes = parseReadonlyTypes;
const fieldRegex = /(.*):([^;]*);?/g;
function parseState(name, content, imports) {
    name = name.trim().split(" ")[0];
    const matches = array_tools_1.regexToArray(fieldRegex, content);
    const fields = matches.map(match => createField_1.createField(match[1], match[2], imports));
    return {
        name,
        fields
    };
}
exports.parseState = parseState;
//# sourceMappingURL=parseStates.js.map