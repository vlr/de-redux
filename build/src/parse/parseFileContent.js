"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const parseImports_1 = require("./impexp/parseImports");
const parseStates_1 = require("./parseStates");
const parseReductions_1 = require("./parseReductions");
const map_tools_1 = require("@vlr/map-tools");
const splitPath_1 = require("../tools/splitPath");
const stripPath_1 = require("../tools/stripPath");
function parseFileContent(file, content, tsconfig) {
    const split = splitPath_1.splitPath(file);
    const imports = map_tools_1.map(parseImports_1.parseImports(tsconfig, content, split.path), i => i.aliasName);
    return {
        path: split.path,
        fileName: stripPath_1.stripExtension(split.file),
        states: parseStates_1.parseStates(content, imports),
        reductions: parseReductions_1.parseReductions(content, imports)
    };
}
exports.parseFileContent = parseFileContent;
//# sourceMappingURL=parseFileContent.js.map