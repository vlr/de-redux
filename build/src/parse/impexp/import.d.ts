export interface Import {
    typeName: string;
    aliasName: string;
    realPath?: string;
    sameFile: boolean;
}
