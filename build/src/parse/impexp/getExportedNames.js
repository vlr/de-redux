"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const array_tools_1 = require("@vlr/array-tools");
function containsReexports(content) {
    return getExportedNames(content).length > 0;
}
exports.containsReexports = containsReexports;
const funcRegex = /export[\s]*function[\s]*([^\s^\()]*)|export[\s]*async[\s]*function[\s]*([^\s^\()]*)/g;
const typeRegex = /export[\s]*enum[\s]*([^\s^{]*)|export[\s]*type[\s]*([^\s^{]*)/g;
const classRegex = /export[\s]*interface[\s]*([^\s^{]*)|export[\s]*class[\s]*([^\s^{]*)/g;
const constRegex = /export[\s]*const[\s]*([^\s]*)[\s]*[:[\s]*[^\s]*[\s]*]?=/g;
function getExportedNames(content) {
    const funcs = array_tools_1.regexToArray(funcRegex, content).map(match => match[1] || match[2]);
    const types = array_tools_1.regexToArray(typeRegex, content).map(match => match[1] || match[2]);
    const classes = array_tools_1.regexToArray(classRegex, content).map(match => match[1] || match[2]);
    const constants = array_tools_1.regexToArray(constRegex, content).map(match => match[1]);
    return [...funcs, ...types, ...classes, ...constants];
}
exports.getExportedNames = getExportedNames;
//# sourceMappingURL=getExportedNames.js.map