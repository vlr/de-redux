"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const calculateRealPath_1 = require("../path/calculateRealPath");
const array_tools_1 = require("@vlr/array-tools");
function parseImports(tsConfig, content, path) {
    return internalImports(content).concat(externalImports(tsConfig, content, path));
}
exports.parseImports = parseImports;
const exportRegex = /export interface (.*) {/g;
function internalImports(content) {
    const matches = array_tools_1.regexToArray(exportRegex, content);
    return matches.map(match => ({ typeName: match[1], aliasName: match[1], sameFile: true }));
}
const importRegex = /import[\s]*{(.*)}[\s]*from[\s]*["|"](.*)["|"]/g;
function externalImports(tsConfig, content, path) {
    const matches = array_tools_1.regexToArray(importRegex, content);
    return array_tools_1.flatMap(matches, match => parseMatch(tsConfig, match[1], match[2], path));
}
function parseMatch(tsConfig, types, importline, path) {
    const realPath = calculateRealPath_1.calculateRealPath(tsConfig, path, importline);
    return types.split(",").map(type => createImport(type, realPath)).filter(x => x);
}
function createImport(type, realPath) {
    const parts = type.split(" ").filter(s => s.length);
    if (!parts.length) {
        return null;
    }
    const index = parts.length === 3 && parts[1] === "as" ? 2 : 0;
    return {
        typeName: parts[0],
        aliasName: parts[index],
        realPath,
        sameFile: false
    };
}
//# sourceMappingURL=parseImports.js.map