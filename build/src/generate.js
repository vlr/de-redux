"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const parseFiles_1 = require("./parse/parseFiles");
const options_1 = require("./options");
const createTree_1 = require("./createTree/createTree");
const defaultOptions = {
    lineFeed: "\r\n",
    IdType: options_1.IdType.number
};
async function generate(paths, options = {}, tsconfig = {}) {
    options = Object.assign({}, defaultOptions, options);
    const parsed = await parseFiles_1.parseFiles(paths, tsconfig);
    const tree = createTree_1.createTree(parsed);
    console.log(tree);
}
exports.generate = generate;
//# sourceMappingURL=generate.js.map