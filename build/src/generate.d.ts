import { Options } from "./options";
export declare function generate(paths: string | string[], options?: Options, tsconfig?: any): Promise<void>;
