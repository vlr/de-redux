"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const combinePath_1 = require("../tools/combinePath");
function filePath(file) {
    return combinePath_1.combinePath(file.path, file.fileName);
}
exports.filePath = filePath;
//# sourceMappingURL=filePath.js.map