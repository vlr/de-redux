import { Parameter } from "../../parse/parsedModel";
import { StateField } from "../tree";
export declare function createStateFields(parsed: Parameter[], allIds: Set<string>, fileStates: string[]): StateField[];
