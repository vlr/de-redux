"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const createStateId_1 = require("../createStateId");
const createPlainFields_1 = require("./createPlainFields");
const createStateFields_1 = require("./createStateFields");
function createState(state, file, allIds) {
    const fileStates = file.states.map(state => state.name);
    const plainFields = createPlainFields_1.createPlainFields(state.fields, allIds, fileStates);
    const stateFields = createStateFields_1.createStateFields(state.fields, allIds, fileStates);
    return {
        name: state.name,
        stateId: createStateId_1.createStateId(state, file),
        plainFields,
        stateFields
    };
}
exports.createState = createState;
//# sourceMappingURL=createState.js.map