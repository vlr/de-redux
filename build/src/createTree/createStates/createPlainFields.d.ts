import { Parameter } from "../../parse/parsedModel";
import { PlainField } from "../tree";
export declare function createPlainFields(parsed: Parameter[], allIds: Set<string>, fileStates: string[]): PlainField[];
