"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const combinePath_1 = require("../../tools/combinePath");
function paramStateId(param) {
    return combinePath_1.combinePath(param.imports[0].typeName, param.typeName);
}
exports.paramStateId = paramStateId;
//# sourceMappingURL=paramStateId.js.map