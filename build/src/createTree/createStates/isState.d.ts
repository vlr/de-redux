import { Parameter } from "../../parse/parsedModel";
export declare function isState(parameter: Parameter, idsSet: Set<string>, fileStates: string[]): boolean;
