"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const createState_1 = require("./createState");
const createStateId_1 = require("../createStateId");
const array_tools_1 = require("@vlr/array-tools");
function createStates(parsed) {
    const allIds = getAllStateIds(parsed);
    return array_tools_1.flatMap(parsed.stateFiles, file => file.states.map(state => createState_1.createState(state, file, allIds)));
}
exports.createStates = createStates;
function getAllStateIds(parsed) {
    const stateIds = array_tools_1.flatMap(parsed.stateFiles, file => file.states.map(state => createStateId_1.createStateId(state, file)));
    return new Set(stateIds);
}
//# sourceMappingURL=createStates.js.map