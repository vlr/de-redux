"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const isComplexType_1 = require("../../tools/isComplexType");
const paramStateId_1 = require("./paramStateId");
function isState(parameter, idsSet, fileStates) {
    if (isComplexType_1.isComplexType(parameter.typeName)) {
        return false;
    }
    switch (parameter.imports.length) {
        case 0:
            return false;
        case 1:
            return isSameFileState(parameter) || isImportedState(parameter, idsSet);
        default:
            throw new Error(`non-complex type ${parameter.typeName} in parameter ${parameter.name} has more than one imports`);
    }
}
exports.isState = isState;
function isSameFileState(parameter) {
    return parameter.imports[0].sameFile;
}
function isImportedState(parameter, idsSet) {
    return idsSet.has(paramStateId_1.paramStateId(parameter));
}
//# sourceMappingURL=isState.js.map