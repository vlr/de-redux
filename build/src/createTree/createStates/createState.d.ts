import { State } from "../tree";
import { ParsedState, ParsedStatesFile } from "../../parse/parsedModel";
export declare function createState(state: ParsedState, file: ParsedStatesFile, allIds: Set<string>): State;
