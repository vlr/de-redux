"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const isState_1 = require("./isState");
const paramStateId_1 = require("./paramStateId");
function createStateFields(parsed, allIds, fileStates) {
    return parsed.filter(param => isState_1.isState(param, allIds, fileStates))
        .map(param => createStateField(param));
}
exports.createStateFields = createStateFields;
function createStateField(param) {
    return Object.assign({ stateId: paramStateId_1.paramStateId(param) }, param);
}
//# sourceMappingURL=createStateFields.js.map