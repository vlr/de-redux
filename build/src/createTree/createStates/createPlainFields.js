"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const isState_1 = require("./isState");
function createPlainFields(parsed, allIds, fileStates) {
    return parsed.filter(param => isState_1.isState(param, allIds, fileStates))
        .map(param => param);
}
exports.createPlainFields = createPlainFields;
//# sourceMappingURL=createPlainFields.js.map