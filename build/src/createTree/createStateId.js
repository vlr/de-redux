"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const filePath_1 = require("./filePath");
const combinePath_1 = require("../tools/combinePath");
function createStateId(state, file) {
    return combinePath_1.combinePath(filePath_1.filePath(file), state.name);
}
exports.createStateId = createStateId;
//# sourceMappingURL=createStateId.js.map