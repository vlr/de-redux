import { Tree } from "./tree";
import { ParsedModel } from "../parse/parsedModel";
export declare function createTree(parsed: ParsedModel): Tree;
