import { ParsedStatesFile, ParsedState } from "../parse/parsedModel";
export declare function createStateId(state: ParsedState, file: ParsedStatesFile): string;
