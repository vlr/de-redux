export interface Options {
    lineFeed?: string;
    IdType?: IdType;
}
export declare enum IdType {
    number = "number",
    uuid = "uuid",
}
