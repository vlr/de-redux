import { IAction } from "../../../src";
import * as actions from "./company-state.actions";
import * as reduction from "../company.reduction";
import { CompanyState } from "../company.state";
import * as userStateReducer from "../../user/gen/user-state.reducer";
import * as userStateActions from "../../user/gen/user-state.actions";
import * as departmentStateReducer from "../department/gen/department-state.reducer";
import * as departmentStateActions from "../department/gen/department-state.actions";
import { UserStateAlter } from "../../user/gen/user-state.alter";

export function companyStateInit_1(): CompanyState {
  return {
    stateId: "1",
    name: null,
    employeeCount: 0,
    department: departmentStateReducer.departmentStateInit_1(),
    user: userStateReducer.userStateInit("2"),
    user2: userStateReducer.userStateInit("3")
  };
}

export function companyStateInit_2(): CompanyState {
  return {
    stateId: "2",
    name: null,
    employeeCount: 0,
    department: departmentStateReducer.departmentStateInit_2(),
    user: userStateReducer.userStateInit("4"),
    user2: userStateReducer.userStateInit("5")
  };
}

export function companyStateReducer(prev: CompanyState, action: IAction): CompanyState {
  return prev ? companyStateSwitch(prev, action) : companyStateInit_1();
}

function companyStateSwitch(prev: CompanyState, action: IAction): CompanyState {
  switch (action.type) {
    case actions.UPDATE_COMPANY_AND_DEPARTMENT: {
      let a = action as actions.UpdateCompanyAndDeprtmentAction;
      return updateCompanyAndDepartment(prev, a.employeeCount, a.departmentName);
    }
    case departmentStateActions.UPDATE_DEPARTMENT_NAME: {
      let a = action as departmentStateActions.UpdateDepartmentNameAction;
      return companyState_departmentState_updateDepartmentName(prev, a.name);
    }
    case userStateActions.ALTER_USER_STATE: {
      let a = action as userStateActions.AlterUserStateAction;
      switch (a.stateId) {
        case "0":
          return companyState_userState_alterUserState_0(prev, a.alter);
        case "2":
          return companyState_userState_alterUserState_1(prev, a.alter);
        case "3":
          return companyState_userState_alterUserState_2(prev, a.alter);
        case "6":
          return companyState_userState_alterUserState_3(prev, a.alter);
        default:
          return prev;
      }
    }
    default:
      return prev;
  }
}

export const updateCompanyAndDepartment = reduction.updateCompanyAndDepartment;

export function companyState_departmentState_updateDepartmentName(prev: CompanyState, name: string): CompanyState {
  return {
    ...prev,
    department: departmentStateReducer.updateDepartmentName(prev.department, name)
  };
}

export function companyState_userState_alterUserState_0(prev: CompanyState, alter: UserStateAlter): CompanyState {
  return {
    ...prev,
    user: userStateReducer.alterUserState(prev.user, alter),
    user2: userStateReducer.alterUserState(prev.user2, alter),
    department: departmentStateReducer.departmentState_userState_alterUserState(prev.department, alter)
  };
}

export function companyState_userState_alterUserState_1(prev: CompanyState, alter: UserStateAlter): CompanyState {
  return {
    ...prev,
    user: userStateReducer.alterUserState(prev.user, alter)
  };
}

export function companyState_userState_alterUserState_2(prev: CompanyState, alter: UserStateAlter): CompanyState {
  return {
    ...prev,
    user2: userStateReducer.alterUserState(prev.user2, alter)
  };
}

export function companyState_userState_alterUserState_3(prev: CompanyState, alter: UserStateAlter): CompanyState {
  return {
    ...prev,
    department: departmentStateReducer.departmentState_userState_alterUserState(prev.department, alter)
  };
}
