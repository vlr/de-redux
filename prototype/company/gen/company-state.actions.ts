import { IAction } from "../../../src";

export const UPDATE_COMPANY_AND_DEPARTMENT = "CompanyState - updateCompanyAndDepartment";
export class UpdateCompanyAndDeprtmentAction implements IAction {
  public type: string = UPDATE_COMPANY_AND_DEPARTMENT;
  constructor(public stateId: string, public employeeCount: number, public departmentName: string) { }
}
