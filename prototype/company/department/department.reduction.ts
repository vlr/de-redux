import { DepartmentState } from "./department.state";

export function updateDepartmentName(prev: DepartmentState, name: string): DepartmentState {
  return {
    ...prev,
    name
  };
}
