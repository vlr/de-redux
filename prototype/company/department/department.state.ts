import { UserState } from "../../user/user.state";

export interface DepartmentState {
  stateId: string;
  name: string;
  personnel: number;
  user: UserState;
}
