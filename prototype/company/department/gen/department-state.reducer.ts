import { DepartmentState } from "../department.state";
import * as actions from "./department-state.actions";
import * as userStateActions from "../../../user/gen/user-state.actions";
import * as reduction from "../department.reduction";
import { IAction } from "../../../../src";
import * as userStateReducer from "../../../user/gen/user-state.reducer";
import { UserStateAlter } from "../../../user/gen/user-state.alter";

export function departmentStateInit_1(): DepartmentState {
  return {
    stateId: "1",
    name: null,
    personnel: 0,
    user: userStateReducer.userStateInit("6")
  };
}

export function departmentStateInit_2(): DepartmentState {
  return {
    stateId: "1",
    name: null,
    personnel: 0,
    user: userStateReducer.userStateInit("7")
  };
}

export function departmentStateReducer(prev: DepartmentState, action: IAction): DepartmentState {
  return prev ? departmentStateSwitch(prev, action) : departmentStateInit_1();
}

function departmentStateSwitch(prev: DepartmentState, action: IAction): DepartmentState {
  switch (action.type) {
    case actions.UPDATE_DEPARTMENT_NAME: {
      let a = action as actions.UpdateDepartmentNameAction;
      return updateDepartmentName(prev, a.name);
    }
    case userStateActions.ALTER_USER_STATE: {
      let a = action as userStateActions.AlterUserStateAction;
      return departmentState_userState_alterUserState(prev, a.alter);
    }
    case userStateActions.DISABLE_USER: {
      return deparmentState_userState_disableUser(prev);
    }
    default:
      return prev;
  }
}

export const updateDepartmentName = reduction.updateDepartmentName;
export function departmentState_userState_alterUserState(prev: DepartmentState, alter: UserStateAlter): DepartmentState {
  return {
    ...prev,
    user: userStateReducer.alterUserState(prev.user, alter)
  };
}

export function deparmentState_userState_disableUser(prev: DepartmentState): DepartmentState {
  return {
    ...prev,
    user: userStateReducer.disableUser(prev.user)
  };
}
