import { IAction } from "../../../../src";

export const UPDATE_DEPARTMENT_NAME = "DepartmentState - updateDepartmentName";
export class UpdateDepartmentNameAction implements IAction {
  public type: string = UPDATE_DEPARTMENT_NAME;
  constructor(public stateId: string, public name: string) { }
}
