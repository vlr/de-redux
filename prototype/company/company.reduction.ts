import { CompanyState } from "./company.state";
import { updateDepartmentName } from "./department/department.reduction";

export function updateCompanyAndDepartment(prev: CompanyState, employeeCount: number, departmentName: string): CompanyState {
  return {
    ...prev,
    employeeCount,
    department: updateDepartmentName(prev.department, departmentName)
  };
}
