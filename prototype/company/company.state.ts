import { UserState } from "../user/user.state";
import { DepartmentState } from "./department/department.state";

export interface CompanyState {
  stateId: string;
  name: string;
  employeeCount: number;
  user: UserState;
  user2: UserState;
  department: DepartmentState;
}
