import { UserState } from "./user/user.state";
import { CompanyState } from "./company/company.state";

export interface RootState {
  user: UserState;
  company: CompanyState;
  company2: CompanyState;
}
