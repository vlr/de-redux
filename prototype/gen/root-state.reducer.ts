/* tslint:disable */
import * as companyStateActions from "../company/gen/company-state.actions";
import * as companyStateReducer from "../company/gen/company-state.reducer";
import { UserStateAlter } from "../user/gen/user-state.alter";
import * as userStateActions from "../user/gen/user-state.actions";
import * as userStateReducer from "../user/gen/user-state.reducer";
import * as departmentStateActions from "../company/department/gen/department-state.actions";
import * as departmentStateReducer from "../company/department/gen/department-state.reducer";
import { RootState } from "../root.state";
import { IAction } from "../../src";

export function rootStateInit(): RootState {
  return {
    company: companyStateReducer.companyStateInit_1(),
    company2: companyStateReducer.companyStateInit_2(),
    user: userStateReducer.userStateInit("1")
  };
}

export function rootStateReducer(prev: RootState, action: IAction): RootState {
  switch (action.type) {
    case companyStateActions.UPDATE_COMPANY_AND_DEPARTMENT: {
      let a = action as companyStateActions.UpdateCompanyAndDeprtmentAction;
      switch (a.stateId) {
        case "0":
          return rootState_companyState_updateCompanyAndDepartment_0(prev, a.employeeCount, a.departmentName);
        case "1":
          return rootState_companyState_updateCompanyAndDepartment_1(prev, a.employeeCount, a.departmentName);
        case "2":
          return rootState_companyState_updateCompanyAndDepartment_2(prev, a.employeeCount, a.departmentName);
        default:
          return prev;
      }
    }
    case departmentStateActions.UPDATE_DEPARTMENT_NAME: {
      let a = action as departmentStateActions.UpdateDepartmentNameAction;
      switch (a.stateId) {
        case "0":
          return rootState_departmentState_updateDepartmentName_0(prev, a.name);
        case "1":
          return rootState_departmentState_updateDepartmentName_1(prev, a.name);
        case "2":
          return rootState_departmentState_updateDepartmentName_2(prev, a.name);
        default:
          return prev;
      }
    }

    case userStateActions.ALTER_USER_STATE: {
      let a = action as userStateActions.AlterUserStateAction;
      switch (a.stateId) {
        case "0":
          return rootState_userState_alterUserState_0(prev, a.alter);
        case "1":
          return rootState_userState_alterUserState_1(prev, a.alter);
        case "2":
          return rootState_userState_alterUserState_2(prev, a.alter);
        case "3":
          return rootState_userState_alterUserState_3(prev, a.alter);
        case "4":
          return rootState_userState_alterUserState_4(prev, a.alter);
        case "5":
          return rootState_userState_alterUserState_5(prev, a.alter);
        case "6":
          return rootState_userState_alterUserState_6(prev, a.alter);
        case "7":
          return rootState_userState_alterUserState_7(prev, a.alter);
        default:
          return prev;
      };
    }
    default:
      return prev;
  }
}

export function rootState_companyState_updateCompanyAndDepartment_0(prev: RootState, employeeCount: number, departmentName: string): RootState {
  return {
    ...prev,
    company: companyStateReducer.updateCompanyAndDepartment(prev.company, employeeCount, departmentName),
    company2: companyStateReducer.updateCompanyAndDepartment(prev.company2, employeeCount, departmentName)
  };
}

export function rootState_companyState_updateCompanyAndDepartment_1(prev: RootState, employeeCount: number, departmentName: string): RootState {
  return {
    ...prev,
    company: companyStateReducer.updateCompanyAndDepartment(prev.company, employeeCount, departmentName)
  };
}

export function rootState_companyState_updateCompanyAndDepartment_2(prev: RootState, employeeCount: number, departmentName: string): RootState {
  return {
    ...prev,
    company2: companyStateReducer.updateCompanyAndDepartment(prev.company2, employeeCount, departmentName)
  };
}

export function rootState_departmentState_updateDepartmentName_0(prev: RootState, name: string): RootState {
  return {
    ...prev,
    company: companyStateReducer.companyState_departmentState_updateDepartmentName(prev.company, name),
    company2: companyStateReducer.companyState_departmentState_updateDepartmentName(prev.company2, name)
  };
}

export function rootState_departmentState_updateDepartmentName_1(prev: RootState, name: string): RootState {
  return {
    ...prev,
    company: companyStateReducer.companyState_departmentState_updateDepartmentName(prev.company, name)
  };
}

export function rootState_departmentState_updateDepartmentName_2(prev: RootState, name: string): RootState {
  return {
    ...prev,
    company2: companyStateReducer.companyState_departmentState_updateDepartmentName(prev.company2, name)
  };
}

export function rootState_userState_alterUserState_0(prev: RootState, alter: UserStateAlter): RootState {
  return {
    ...prev,
    company: companyStateReducer.companyState_userState_alterUserState_0(prev.company, alter),
    company2: companyStateReducer.companyState_userState_alterUserState_0(prev.company2, alter),
    user: userStateReducer.alterUserState(prev.user, alter)
  };
}

export function rootState_userState_alterUserState_1(prev: RootState, alter: UserStateAlter): RootState {
  return {
    ...prev,
    user: userStateReducer.alterUserState(prev.user, alter)
  };
}

export function rootState_userState_alterUserState_2(prev: RootState, alter: UserStateAlter): RootState {
  return {
    ...prev,
    company: companyStateReducer.companyState_userState_alterUserState_1(prev.company, alter)
  };
}

export function rootState_userState_alterUserState_3(prev: RootState, alter: UserStateAlter): RootState {
  return {
    ...prev,
    company: companyStateReducer.companyState_userState_alterUserState_2(prev.company, alter)
  };
}

export function rootState_userState_alterUserState_4(prev: RootState, alter: UserStateAlter): RootState {
  return {
    ...prev,
    company2: companyStateReducer.companyState_userState_alterUserState_1(prev.company2, alter)
  };
}

export function rootState_userState_alterUserState_5(prev: RootState, alter: UserStateAlter): RootState {
  return {
    ...prev,
    company2: companyStateReducer.companyState_userState_alterUserState_2(prev.company2, alter)
  };
}

export function rootState_userState_alterUserState_6(prev: RootState, alter: UserStateAlter): RootState {
  return {
    ...prev,
    company: companyStateReducer.companyState_userState_alterUserState_3(prev.company, alter)
  };
}

export function rootState_userState_alterUserState_7(prev: RootState, alter: UserStateAlter): RootState {
  return {
    ...prev,
    company2: companyStateReducer.companyState_userState_alterUserState_3(prev.company2, alter)
  };
}
