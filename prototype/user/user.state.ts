import { UserAddress } from "./user-address";

export interface UserState {
  stateId: string;
  name?: string;
  isDisabled: boolean;
  addresses: UserAddress[];
}
