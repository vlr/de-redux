import { UserState } from "./user.state";

export function init(state: UserState): UserState {
  return {
    ...state,
    name: "default name"
  };
}

export function disableUser(prev: UserState): UserState {
  return {
    ...prev,
    isDisabled: true
  };
}
