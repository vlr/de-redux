import { UserState } from "../user.state";
import * as reduction from "../user.reduction";
import { IAction } from "../../../src";
import * as actions from "./user-state.actions";
import { UserStateAlter } from "./user-state.alter";

export function userStateInit(stateId: string): UserState {
  return reduction.init({
    stateId,
    userId: null,
    name: null,
    age: 0,
    isDisabled: false
  });
}

export function userStateReducer(prev: UserState, action: IAction): UserState {
  return prev ? userStateSwitch(prev, action) : userStateInit("1");
}

function userStateSwitch(prev: UserState, action: IAction): UserState {
  switch (action.type) {
    case actions.UPDATE_USER_NAME: {
      let a = action as actions.UpdateUserNameAction;
      return updateUserName(prev, a.name);
    }
    case actions.DISABLE_USER: {
      return disableUser(prev);
    }
    case actions.ALTER_USER_STATE: {
      let a = action as actions.AlterUserStateAction;
      return alterUserState(prev, a.alter);
    }
    default:
      return prev;
  }
}

export function alterUserState(prev: UserState, alter: UserStateAlter): UserState {
  return {
    ...prev,
    ...alter
  };
}

export const updateUserName = reduction.updateUserName;
export const disableUser = reduction.disableUser;