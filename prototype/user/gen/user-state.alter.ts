export interface UserStateAlter {
  userId?: string;
  name?: string;
  age?: number;
  isDisabled?: boolean;
}
