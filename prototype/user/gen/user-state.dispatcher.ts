import { IDispatcher } from "../../../src";
import { CompanyState } from "../../company/company.state";
import { RootState } from "../../root.state";
import { UserState } from "../user.state";
import * as actions from "./user-state.actions";
import { UserStateAlter } from "./user-state.alter";

export function userStateFromCompanyStateSelector(stateId: string): (company: CompanyState) => UserState {
  return company => company.user;
}

export function userStateFromRootStateSelector(stateId: string): (rootState: RootState) => UserState {
  switch (stateId) {
    case "1":
      return rootState => rootState.user;
    case "2":
      return rootState => rootState.company.user;
    case "3":
      return rootState => rootState.company.user2;
    case "4":
      return rootState => rootState.company2.user;
    case "5":
      return rootState => rootState.company2.user2;
    case "6":
      return rootState => rootState.company.department.user;
    case "7":
      return rootState => rootState.company2.department.user;
    default:
      throw new Error(`Id ${stateId} is not present in the tree`);
  }
}

export class UserStateIdDispatcher {
  constructor(private dispatcher: IDispatcher) { }

  public enrollUser(stateId: string): void {
    this.dispatcher.dispatch(new actions.DisableUserAction(stateId));
  }

  public alterUserState(stateId: string, alter: UserStateAlter): void {
    this.dispatcher.dispatch(new actions.AlterUserStateAction(stateId, alter));
  }
}

export class UserStateDispatcher {
  constructor(private dispatcher: IDispatcher, private stateId: string = "0") { }

  public enrollUser(): void {
    this.dispatcher.dispatch(new actions.DisableUserAction(this.stateId));
  }

  public alterUserState(alter: UserStateAlter): void {
    this.dispatcher.dispatch(new actions.AlterUserStateAction(this.stateId, alter));
  }
}
