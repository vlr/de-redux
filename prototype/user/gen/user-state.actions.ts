import { IAction } from "../../../src";
import { UserStateAlter } from "./user-state.alter";

export const DISABLE_USER = "UserState - disableUser";
export class DisableUserAction implements IAction {
  public type: string = DISABLE_USER;
  constructor(public stateId: string) { }
}

export const ALTER_USER_STATE = "UserState = alterUserState";
export class AlterUserStateAction implements IAction {
  public type: string = ALTER_USER_STATE;
  constructor(public stateId: string, public alter: UserStateAlter) { }
}
