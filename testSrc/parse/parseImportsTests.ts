import { parseImports } from "../../src/parse/impexp/parseImports";
import { Import } from "../../src/parse/impexp/import";
import { expect } from "chai";

describe("parseImports", function (): void {
  it("should return singular imports", async function (): Promise<void> {
    // arrange
    const path = "./someDir/dir2";

    const content = `
    import { Type1 } from ".";
    import { Type2 } from ".";
`;
    const expected: Import[] = [
      { typeName: "Type1", aliasName: "Type1", realPath: "./someDir/dir2", sameFile: false },
      { typeName: "Type2", aliasName: "Type2", realPath: "./someDir/dir2", sameFile: false }
    ];

    // act
    const result = parseImports({}, content, path);

    // assert
    checkImports(result, expected);
  });

  it("should return multi imports", async function (): Promise<void> {
    // arrange
    const path = "./someDir/dir2";

    const content = `
    import { Type1, Type2 } from ".";
`;
    const expected: Import[] = [
      { typeName: "Type1", aliasName: "Type1", realPath: "./someDir/dir2", sameFile: false },
      { typeName: "Type2", aliasName: "Type2", realPath: "./someDir/dir2", sameFile: false }
    ];

    // act
    const result = parseImports({}, content, path);

    // assert
    checkImports(result, expected);
  });

  it("should add same file interfaces", async function (): Promise<void> {
    // arrange
    const path = "./someDir/dir2";

    const content = `
        export interface SomeType1 {
        }

        export interface SomeType2 {

        }
    `;
    const expected: Import[] = [
      { typeName: "SomeType1", aliasName: "SomeType1", sameFile: true },
      { typeName: "SomeType2", aliasName: "SomeType2", sameFile: true }
    ];
    // act
    const result = parseImports({}, content, path);

    // assert
    checkImports(result, expected);
  });
});

export function checkImports(imports: Import[], expected: Import[]): void {
  expect(imports).to.be.not.equal(undefined);
  expect(imports.length).equals(expected.length);
  for (let exp of expected) {
    const imp = imports.find(i => i.realPath === exp.realPath && i.typeName === exp.typeName);
    expect(imp).to.be.not.equal(undefined);
    expect(imp.aliasName).equals(exp.aliasName);
    expect(imp.sameFile).equals(exp.sameFile);
  }
}
