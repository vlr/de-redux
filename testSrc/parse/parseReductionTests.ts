import { parseReductions } from "../../src/parse/parseReductions";
import { expect } from "chai";
import { Import } from "../../src/parse/impexp/import";

describe("parseReduction", function (): void {
  it("should parse partial reductions", async function (): Promise<void> {
    const imports = new Map<string, Import>();
    const imp = { typeName: "CompanyState", aliasName: "CompanyState", realPath: "./", sameFile: false };
    imports.set("CompanyState", imp);
    const content = `
export function companyReductions(prev: CompanyState) {
  function updateCompanyAndDepartment(employeeCount: number, departmentName: string): CompanyState {
      return {
        ...prev,
        employeeCount,
        department: updateDepartmentName(prev.department, departmentName)
      };
    }

    function updateEmployeeCount(employeeCount: number){
      return {
        ...prev,
        employeeCount
      };
    }
    return {
      updateCompanyAndDepartment,
      updateEmployeeCount
    };
}`;

    const fields1 = ["employeeCount", "departmentName"];
    const types1 = ["number", "string"];

    const fields2 = [, "employeeCount"];
    const types2 = ["number"];

    // act
    const result = parseReductions(content, imports);

    // assert
    expect(result[0].partialName).equals("companyReductions");
    expect(result[0].name).equals("updateCompanyAndDepartment");
    expect(result[0].parameters.map(f => f.name)).to.be.deep.equal(fields1);
    expect(result[0].parameters.map(f => f.typeName)).to.be.deep.equal(types1);
    expect(result[0].reducted).equals(imp);

    expect(result[1].partialName).equals("companyReductions");
    expect(result[1].name).equals("updateEmployeeCount");
    expect(result[1].parameters.map(f => f.name)).to.be.deep.equal(fields2);
    expect(result[1].parameters.map(f => f.typeName)).to.be.deep.equal(types2);
    expect(result[1].reducted).equals(imp);
  });

  it("should parse reduction", async function (): Promise<void> {
    // arrange
    const imports = new Map<string, Import>();
    const imp = { typeName: "CompanyState", aliasName: "CompanyState", realPath: "./", sameFile: false };
    imports.set("CompanyState", imp);
    const content = `
    export function updateCompanyAndDepartment(prev: CompanyState, employeeCount: number, departmentName: string): CompanyState {
      return {
        ...prev,
        employeeCount,
        department: updateDepartmentName(prev.department, departmentName)
      };
    }`;
    const fields = ["employeeCount", "departmentName"];
    const types = ["number", "string"];

    // act
    const result = parseReductions(content, imports)[0];

    // assert
    expect(result.partialName).equals(null);
    expect(result.reducted).equals(imp);
    expect(result.name).equals("updateCompanyAndDepartment");
    expect(result.parameters.map(f => f.name)).to.be.deep.equal(fields);
    expect(result.parameters.map(f => f.typeName)).to.be.deep.equal(types);
  });

  it("should ignore function if it returns not the same type as first parameter", async function (): Promise<void> {
    // arrange
    const imports = new Map<string, Import>();
    const content = `
    export function updateCompanyAndDepartment(prev: CompanyState): UserState {
    }`;

    // act
    const result = await parseReductions(content, imports);

    // assert
    expect(result.length).equals(0);
  });

  it("should ignore function if no parameters", async function (): Promise<void> {
    // arrange
    const imports = new Map<string, Import>();
    const content = `
    export function updateCompanyAndDepartment(): UserState {
    }`;

    // act
    const result = await parseReductions(content, imports);

    // assert
    expect(result.length).equals(0);
  });

  it("should ignore function if parameter type not equals return parameter", async function (): Promise<void> {
    // arrange
    const imports = new Map<string, Import>();
    const content = `
    export function updateCompanyAndDepartment(prev: OtherState): UserState {
    }`;

    // act
    const result = parseReductions(content, imports);

    // assert
    expect(result.length).equals(0);
  });
});
