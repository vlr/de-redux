import { parseStates } from "../../src/parse/parseStates";
import { expect } from "chai";
import { Import } from "../../src/parse/impexp/import";

describe("parseStates", function (): void {
  it("should parse plain interface", async function (): Promise<void> {
    // arrange
    const imports = new Map<string, Import>();
    const content = `
    export interface UserState {
      stateId: string;
      name?: string;
      isDisabled: boolean;
      addresses: UserAddress[];
    }
    `;
    const fields = ["stateId", "name", "isDisabled", "addresses"];
    const types = ["string", "string", "boolean", "UserAddress[]"];

    // act
    const result = parseStates(content, imports)[0];

    // assert
    expect(result.name).equals("UserState");
    expect(result.fields.map(f => f.name)).to.be.deep.equal(fields);
    expect(result.fields.map(f => f.typeName)).to.be.deep.equal(types);
  });

  it("should parse readonly type", async function (): Promise<void> {
    // arrange
    const imports = new Map<string, Import>();
    const content = `
    export type UserState = Readonly<{
      stateId: string;
      name?: string;
      isDisabled: boolean;
      addresses: UserAddress[];
    }>;
    `;
    const fields = ["stateId", "name", "isDisabled", "addresses"];
    const types = ["string", "string", "boolean", "UserAddress[]"];

    // act
    const result = parseStates(content, imports)[0];

    // assert
    expect(result.name).equals("UserState");
    expect(result.fields.map(f => f.name)).to.be.deep.equal(fields);
    expect(result.fields.map(f => f.typeName)).to.be.deep.equal(types);
  });

  it("should parse recursive readonly type", async function (): Promise<void> {
    // arrange
    const imports = new Map<string, Import>();
    const content = `
    export type UserState = RecursiveReadonly<{
      stateId: string;
      name?: string;
      isDisabled: boolean;
      addresses: UserAddress[];
    }>;
    `;
    const fields = ["stateId", "name", "isDisabled", "addresses"];
    const types = ["string", "string", "boolean", "UserAddress[]"];

    // act
    const result = parseStates(content, imports)[0];

    // assert
    expect(result.name).equals("UserState");
    expect(result.fields.map(f => f.name)).to.be.deep.equal(fields);
    expect(result.fields.map(f => f.typeName)).to.be.deep.equal(types);
  });

  it("should parse extended interface", async function (): Promise<void> {
    // arrange
    const imports = new Map<string, Import>();
    const content = `
    export interface UserState extends BaseState {
      stateId: string;
    }
    `;

    // act
    const result = parseStates(content, imports)[0];

    // assert
    expect(result.name).equals("UserState");
  });

  it("should parse two interfaces", async function (): Promise<void> {
    // arrange
    const imports = new Map<string, Import>();
    const content = `
    export interface UserState {
      stateId: string;
    }

    export interface CompanyState {
      stateId: string;
    }
    `;

    // act
    const result = parseStates(content, imports);

    // assert
    expect(result.map(s => s.name)).to.be.deep.equal(["UserState", "CompanyState"]);
  });
});
