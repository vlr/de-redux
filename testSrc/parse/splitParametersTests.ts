import { splitParameters } from "../../src/parse/splitParameters";
import { expect } from "chai";

describe("splitParameters", function (): void {
  it("should return one simple parameter", async function (): Promise<void> {
    // arrange
    const args = "user: User";
    const expected = [args];
    // act
    const result = await splitParameters(args);

    // assert
    expect(result).to.be.deep.equal(expected);
  });

  it("should return one generic parameter", async function (): Promise<void> {
    // arrange
    const args = "user: User<string, string>";
    const expected = [args];
    // act
    const result = await splitParameters(args);

    // assert
    expect(result).to.be.deep.equal(expected);
  });

  it("should split simple parameter", async function (): Promise<void> {
    // arrange
    const args = "user: User, company: Company, department: Department";
    const expected = ["user: User", "company: Company", "department: Department"];
    // act
    const result = await splitParameters(args);

    // assert
    expect(result).to.be.deep.equal(expected);
  });

  it("should return two generic parameters", async function (): Promise<void> {
    // arrange
    const args = "user: User<bool, string>, company: Company<string, bool>";
    const expected = ["user: User<bool, string>", "company: Company<string, bool>"];
    // act
    const result = await splitParameters(args);

    // assert
    expect(result).to.be.deep.equal(expected);
  });

});
