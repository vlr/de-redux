import { parseFileContent } from "../../src/parse/parseFileContent";
import { ParsedStatesFile, ParsedReductionsFile, ParsedFile } from "../../src/parse/parsedModel";
import { expect } from "chai";

describe("parseFileContent", function (): void {
  it("should return parsed user state", async function (): Promise<void> {
    // arrange
    const content = `
    import { UserAddress } from "./user-address";

export interface UserState {
}
    `;
    const expected = {
      path: "./prototype/user",
      fileName: "user.state",
      count: 1
    };

    // act
    const result = await parseFileContent("./prototype/user/user.state.ts", content, {});

    // assert
    checkStateFile(expected, result);
  });

  it("should return parsed user reductions", async function (): Promise<void> {
    // arrange
    const content = `
    import { UserState } from "./user.state";

export function init(state: UserState): UserState {
  return {
    ...state,
    name: "default name"
  };
}

export function disableUser(prev: UserState): UserState {
  return {
    ...prev,
    isDisabled: true
  };
}
`;
    const expected = {
      path: "./prototype/user",
      fileName: "user.reduction",
      count: 2
    };

    // act
    const result = await parseFileContent("./prototype/user/user.reduction.ts", content, {});

    // assert
    checkReductionFile(expected, result);
  });

});

interface Expected {
  path: string;
  fileName: string;
  count: number;
}

function checkStateFile(expected: Expected, file: ParsedStatesFile): void {
  checkFile(expected, file);
  expect(file.states.length).equals(expected.count);
}

function checkFile(expected: Expected, file: ParsedFile): void {
  expect(file.fileName).equals(expected.fileName);
  expect(file.path).equals(expected.path);
}

function checkReductionFile(expected: Expected, file: ParsedReductionsFile): void {
  checkFile(expected, file);
  expect(file.reductions.length).equals(expected.count);
}
