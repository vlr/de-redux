import { parseFiles } from "../../src/parse/parseFiles";
import { expect } from "chai";

describe("parseFiles", function (): void {
  it("should return user state and reduction", async function (): Promise<void> {
    // arrange
    const userDir = "./prototype/user";

    // act
    const result = await parseFiles(userDir, {});

    // assert
    expect(!!result).equals(true);
    expect(result.stateFiles.length).equals(1);
    expect(result.reductionFiles.length).equals(1);
  });

  it("should return user and department state and reductions", async function (): Promise<void> {
    // arrange
    const userDir = "./prototype/user";
    const departmentDir = "./prototype/company/department";
    const dirs = [userDir, departmentDir];

    // act
    const result = await parseFiles(dirs, {});

    // assert
    expect(!!result).equals(true);
    expect(result.stateFiles.length).equals(2);
    expect(result.reductionFiles.length).equals(2);
  });
});
