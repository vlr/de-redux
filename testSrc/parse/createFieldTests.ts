import { createField } from "../../src/parse/createField";
import { Import } from "../../src/parse/impexp/import";
import { expect } from "chai";

describe("createField", function (): void {
  it("should return isOptional false for name without question mark", async function (): Promise<void> {
    // arrange
    const imports = new Map<string, Import>();

    // act
    const result = createField("flag", "boolean", imports);

    // assert
    expect(result.name).equals("flag");
    expect(result.isOptional).equals(false);
  });

  it("should return isOptional and trim question mark", async function (): Promise<void> {
    // arrange
    const imports = new Map<string, Import>();

    // act
    const result = createField("flag?", "boolean", imports);

    // assert
    expect(result.name).equals("flag");
    expect(result.isOptional).equals(true);
  });
});
