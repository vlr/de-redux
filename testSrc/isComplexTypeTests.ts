import { expect } from "chai";
import { isComplexType } from "../src/tools/isComplexType";

describe("isComplexType", function (): void {
  it("should return false for simple type", async function (): Promise<void> {
    // arrange
    const type = "boolean";

    // act
    const result = isComplexType(type);

    // assert
    expect(result).equals(false);
  });

  it("should return true for array", async function (): Promise<void> {
    // arrange
    const type = "boolean[]";

    // act
    const result = isComplexType(type);

    // assert
    expect(result).equals(true);
  });

  it("should return true for generic type", async function (): Promise<void> {
    // arrange
    const type = "Set<boolean>";

    // act
    const result = isComplexType(type);

    // assert
    expect(result).equals(true);
  });
});
